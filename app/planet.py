from typing import List, Tuple

from app import Direction, HEIGHT, WIDTH
from app.obstacle_exception import ObstacleException


class Planet:
    def __init__(self, obstacles: List[Tuple[int, int]]):
        self.rover_x = 0
        self.rover_y = 0
        self.obstacles = obstacles

    def move_towards(self, direction: Direction) -> None:
        new_x, new_y = self.rover_x, self.rover_y

        if direction == Direction.EAST:
            new_x = (self.rover_x + 1) % WIDTH
        elif direction == Direction.WEST:
            new_x = (self.rover_x - 1 + WIDTH) % WIDTH
        elif direction == Direction.SOUTH:
            new_y = (self.rover_y - 1 + HEIGHT) % HEIGHT
        elif direction == Direction.NORTH:
            new_y = (self.rover_y + 1) % HEIGHT

        if self._is_obstacle_next(new_x, new_y):
            raise ObstacleException()

        self.rover_x, self.rover_y = new_x, new_y

    @property
    def rover_position(self):
        return (self.rover_x, self.rover_y)

    @rover_position.setter
    def rover_position(self, value):
        self.rover_x = value[0]
        self.rover_y = value[1]

    def _is_obstacle_next(self, new_x, new_y):
        return any((new_x, new_y) == (x, y) for (x, y) in self.obstacles)
