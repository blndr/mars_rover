from app.command import Command
from app.direction import Direction


class Rover:
    def __init__(self):
        self.direction = Direction.NORTH
        self.legal_moves = {
            Command.FORWARD: {
                Direction.NORTH: Direction.NORTH,
                Direction.SOUTH: Direction.SOUTH,
                Direction.EAST: Direction.EAST,
                Direction.WEST: Direction.WEST
            },
            Command.BACKWARD: {
                Direction.NORTH: Direction.SOUTH,
                Direction.SOUTH: Direction.NORTH,
                Direction.EAST: Direction.WEST,
                Direction.WEST: Direction.EAST
            },
            Command.LEFT: {
                Direction.NORTH: Direction.WEST,
                Direction.WEST: Direction.SOUTH,
                Direction.SOUTH: Direction.EAST,
                Direction.EAST: Direction.NORTH
            },
            Command.RIGHT: {
                Direction.NORTH: Direction.EAST,
                Direction.EAST: Direction.SOUTH,
                Direction.SOUTH: Direction.WEST,
                Direction.WEST: Direction.NORTH
            }
        }

    def turn(self, command: Command) -> None:
        self.direction = self.legal_moves[command][self.direction]
