from app import WIDTH, HEIGHT
from app.planet import Planet


class Telescope:
    def __init__(self):
        pass

    def observe(self, planet: Planet) -> str:
        grid = [['.'] * HEIGHT for column in range(WIDTH)]
        rover_position = planet.rover_position
        for obstacle in planet.obstacles:
            grid[obstacle[0]][obstacle[1]] = 'O'

        grid[rover_position[0]][rover_position[1]] = 'R'

        image = '\n'
        for row in reversed(range(HEIGHT)):
            image += ' '.join((grid[column][row] for column in range(WIDTH))) + '\n'

        return image
