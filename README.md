# Mars Rover
This is a quite famous coding kata. The rules are :

+ You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W) it is facing.
+ The rover receives a character array of commands.
+ Implement commands that move the rover forward/backward (f,b).
+ Implement commands that turn the rover left/right (l,r).
+ Implement wrapping from one edge of the grid to another. (planets are spheres after all)
+ Implement obstacle detection before each move to a new square. If a given sequence of commands encounters an obstacle, the rover moves up to the last possible point and reports the obstacle.

## Install requirements
This app requires Python 3.6 as it uses the type hints mechanism.
```
virtualenv venv -p /path/to/python/3.6
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest
```