import unittest

from app.command import Command
from app.direction import Direction
from app.rover import Rover


class RoverTest(unittest.TestCase):
    def setup_method(self, method):
        self.rover = Rover()

    def test_rover_is_facing_north_by_default(self):
        assert self.rover.direction == Direction.NORTH

    def test_command_forward_preserves_the_direction_when_going_west(self):
        # given
        self.rover.direction = Direction.WEST

        # when
        self.rover.turn(Command.FORWARD)

        # then
        assert self.rover.direction == Direction.WEST

    def test_command_forward_preserves_the_direction_when_going_east(self):
        # given
        self.rover.direction = Direction.EAST

        # when
        self.rover.turn(Command.FORWARD)

        # then
        assert self.rover.direction == Direction.EAST

    def test_command_forward_preserves_the_direction_when_going_north(self):
        # given
        self.rover.direction = Direction.NORTH

        # when
        self.rover.turn(Command.FORWARD)

        # then
        assert self.rover.direction == Direction.NORTH

    def test_command_forward_preserves_the_direction_when_going_south(self):
        # given
        self.rover.direction = Direction.SOUTH

        # when
        self.rover.turn(Command.FORWARD)

        # then
        assert self.rover.direction == Direction.SOUTH

    def test_command_backward_reverses_the_direction_when_going_west(self):
        # given
        self.rover.direction = Direction.WEST

        # when
        self.rover.turn(Command.BACKWARD)

        # then
        assert self.rover.direction == Direction.EAST

    def test_command_backward_reverses_the_direction_when_going_east(self):
        # given
        self.rover.direction = Direction.EAST

        # when
        self.rover.turn(Command.BACKWARD)

        # then
        assert self.rover.direction == Direction.WEST

    def test_command_backward_reverses_the_direction_when_going_north(self):
        # given
        self.rover.direction = Direction.NORTH

        # when
        self.rover.turn(Command.BACKWARD)

        # then
        assert self.rover.direction == Direction.SOUTH

    def test_command_backward_reverses_the_direction_when_going_south(self):
        # given
        self.rover.direction = Direction.SOUTH

        # when
        self.rover.turn(Command.BACKWARD)

        # then
        assert self.rover.direction == Direction.NORTH

    def test_command_left_turns_the_rover_counter_clockwise_when_going_north(self):
        # given
        self.rover.direction = Direction.NORTH

        # when
        self.rover.turn(Command.LEFT)

        # then
        assert self.rover.direction == Direction.WEST

    def test_command_left_turns_the_rover_counter_clockwise_when_going_west(self):
        # given
        self.rover.direction = Direction.WEST

        # when
        self.rover.turn(Command.LEFT)

        # then
        assert self.rover.direction == Direction.SOUTH

    def test_command_left_turns_the_rover_counter_clockwise_when_going_south(self):
        # given
        self.rover.direction = Direction.SOUTH

        # when
        self.rover.turn(Command.LEFT)

        # then
        assert self.rover.direction == Direction.EAST

    def test_command_left_turns_the_rover_counter_clockwise_when_going_east(self):
        # given
        self.rover.direction = Direction.EAST

        # when
        self.rover.turn(Command.LEFT)

        # then
        assert self.rover.direction == Direction.NORTH

    def test_command_right_turns_the_rover_clockwise_when_going_north(self):
        # given
        self.rover.direction = Direction.NORTH

        # when
        self.rover.turn(Command.RIGHT)

        # then
        assert self.rover.direction == Direction.EAST

    def test_command_right_turns_the_rover_clockwise_when_going_east(self):
        # given
        self.rover.direction = Direction.EAST

        # when
        self.rover.turn(Command.RIGHT)

        # then
        assert self.rover.direction == Direction.SOUTH

    def test_command_right_turns_the_rover_clockwise_when_going_south(self):
        # given
        self.rover.direction = Direction.SOUTH

        # when
        self.rover.turn(Command.RIGHT)

        # then
        assert self.rover.direction == Direction.WEST

    def test_command_right_turns_the_rover_clockwise_when_going_west(self):
        # given
        self.rover.direction = Direction.WEST

        # when
        self.rover.turn(Command.RIGHT)

        # then
        assert self.rover.direction == Direction.NORTH
