import unittest

from app.planet import Planet
from app.telescope import Telescope


class TelescopeTest(unittest.TestCase):
    def setup_method(self, method):
        self.telescope = Telescope()

    def test_observe_returns_a_string_representation_of_the_planet(self):
        # given
        planet = Planet([(1, 1), (7, 4)])
        planet.rover_position = (2, 2)

        # when
        image = self.telescope.observe(planet)

        # then
        assert image == '\n' \
                        '. . . . . . . . .\n' \
                        '. . . . . . . . .\n' \
                        '. . . . . . . . .\n' \
                        '. . . . . . . . .\n' \
                        '. . . . . . . O .\n' \
                        '. . . . . . . . .\n' \
                        '. . R . . . . . .\n' \
                        '. O . . . . . . .\n' \
                        '. . . . . . . . .\n'
