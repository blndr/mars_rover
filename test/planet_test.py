import unittest

from pytest import raises

from app import Direction, HEIGHT, WIDTH
from app.obstacle_exception import ObstacleException
from app.planet import Planet


class PlanetTest(unittest.TestCase):
    def setup_method(self, method):
        self.planet = Planet([])

    def test_initial_rover_position_is_on_x0_y0(self):
        assert self.planet.rover_position == (0, 0)

    def test_move_towards_north_from_x3_y3_updates_rover_position_to_x3_y4(self):
        # given
        self.planet.rover_position = (3, 3)

        # when
        self.planet.move_towards(Direction.NORTH)

        # then
        assert self.planet.rover_position == (3, 4)

    def test_move_towards_east_from_x3_y3_updates_rover_position_to_x4_y3(self):
        # given
        self.planet.rover_position = (3, 3)

        # when
        self.planet.move_towards(Direction.EAST)

        # then
        assert self.planet.rover_position == (4, 3)

    def test_move_towards_west_from_x3_y3_updates_rover_position_to_x2_y3(self):
        # given
        self.planet.rover_position = (3, 3)

        # when
        self.planet.move_towards(Direction.WEST)

        # then
        assert self.planet.rover_position == (2, 3)

    def test_move_towards_south_from_x3_y3_updates_rover_position_to_x3_y2(self):
        # given
        self.planet.rover_position = (3, 3)

        # when
        self.planet.move_towards(Direction.SOUTH)

        # then
        assert self.planet.rover_position == (3, 2)

    def test_move_towards_north_from_top_border_updates_rover_position_to_x3_y0(self):
        # given
        self.planet.rover_position = (3, HEIGHT - 1)

        # when
        self.planet.move_towards(Direction.NORTH)

        # then
        assert self.planet.rover_position == (3, 0)

    def test_move_towards_east_from_right_border_updates_rover_position_to_x0_y3(self):
        # given
        self.planet.rover_position = (WIDTH - 1, 3)

        # when
        self.planet.move_towards(Direction.EAST)

        # then
        assert self.planet.rover_position == (0, 3)

    def test_move_towards_west_from_left_border_updates_rover_position_to_x_right_border_minus_1_y3(self):
        # given
        self.planet.rover_position = (0, 3)

        # when
        self.planet.move_towards(Direction.WEST)

        # then
        assert self.planet.rover_position == (WIDTH - 1, 3)

    def test_move_towards_south_from_bottom_border_updates_rover_position_to_x3_y_top_border_minus_1(self):
        # given
        self.planet.rover_position = (3, 0)

        # when
        self.planet.move_towards(Direction.SOUTH)

        # then
        assert self.planet.rover_position == (3, HEIGHT - 1)

    def test_cannot_move_towards_north_from_x3_y3_because_of_obstacle(self):
        # given
        self.planet.obstacles = [(3, 4)]
        self.planet.rover_position = (3, 3)

        # then
        with raises(ObstacleException):
            self.planet.move_towards(Direction.NORTH)

    def test_cannot_move_towards_west_from_x3_y3_because_of_obstacle(self):
        # given
        self.planet.obstacles = [(2, 3)]
        self.planet.rover_position = (3, 3)

        # then
        with raises(ObstacleException):
            self.planet.move_towards(Direction.WEST)

    def test_cannot_move_towards_east_from_x3_y3_because_of_obstacle(self):
        # given
        self.planet.obstacles = [(4, 3)]
        self.planet.rover_position = (3, 3)

        # then
        with raises(ObstacleException):
            self.planet.move_towards(Direction.EAST)

    def test_cannot_move_towards_south_from_x3_y3_because_of_obstacle(self):
        # given
        self.planet.obstacles = [(3, 2)]
        self.planet.rover_position = (3, 3)

        # then
        with raises(ObstacleException):
            self.planet.move_towards(Direction.SOUTH)

    def test_cannot_move_towards_north_from_top_border_because_of_obstacle(self):
        # given
        self.planet.obstacles = [(3, 0)]
        self.planet.rover_position = (3, HEIGHT - 1)

        # then
        with raises(ObstacleException):
            self.planet.move_towards(Direction.NORTH)

    def test_cannot_move_towards_east_from_right_border_because_of_obstacle(self):
        # given
        self.planet.obstacles = [(0, 3)]
        self.planet.rover_position = (WIDTH - 1, 3)

        # then
        with raises(ObstacleException):
            self.planet.move_towards(Direction.EAST)

    def test_cannot_move_towards_west_from_left_border_because_of_obstacle(self):
        # given
        self.planet.obstacles = [(WIDTH - 1, 3)]
        self.planet.rover_position = (0, 3)

        # then
        with raises(ObstacleException):
            self.planet.move_towards(Direction.WEST)

    def test_cannot_move_towards_south_from_bottom_border_because_of_obstacle(self):
        # given
        self.planet.obstacles = [(3, HEIGHT - 1)]
        self.planet.rover_position = (3, 0)

        # when
        with raises(ObstacleException):
            self.planet.move_towards(Direction.SOUTH)
