#!/usr/bin/env python3
from time import sleep

from app import Command
from app.obstacle_exception import ObstacleException
from app.planet import Planet
from app.rover import Rover
from app.telescope import Telescope

obstacles = [(6, 2), (1, 0)]
planet = Planet(obstacles)
rover = Rover()
telescope = Telescope()

commands = ['F', 'R', 'F', 'R', 'B', 'L', 'R', 'L', 'F', 'F', 'F', 'F']

for command in commands:
    sleep(0.4)
    rover.turn(Command(command))
    try:
        planet.move_towards(rover.direction)
    except ObstacleException:
        print('/!\ Obstacle detected !')
    else:
        print(telescope.observe(planet))
